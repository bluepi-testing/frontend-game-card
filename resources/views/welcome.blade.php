@extends('Layouts.main')
@section('contents')
    <div class="container-fluit p-5">
        <div class="row">
            <div class="col-md-4">
                <div class="row font-weight-bold mt-5">
                    Click:
                </div>
                <div class="row" id="totalClick">
                    -
                </div>

                <div class="row font-weight-bold mt-5">
                    My Best:
                </div>
                <div class="row" id="myBest">
                    -
                </div>

                <div class="row font-weight-bold mt-5">
                    Global Best:
                </div>
                <div class="row" id="globalBest">
                    -
                </div>

                <div class="row mt-5">
                    <button class="btn btn-warning" onclick="newGame()"> New game</button>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    @for($i = 0; $i <= 11; $i++)
                        <div class="scene scene--card col-md-3" onclick="sendKey(this)" data-key="{{$i}}">
                            <div class="card flip{{$i}}"  >
                                <div class="card__face card__face--front is-front-{{$i}}"></div>
                                <div class="card__face card__face--back is-back-{{$i}}"></div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
@endsection