getApi();
window.onbeforeunload = function(){
    Cookies.remove('isBlock')
    Cookies.remove('firstClick');
    Cookies.remove('secondClick');
    Cookies.remove('gameId');
    Cookies.remove('gameSessionHash');
};


function initFingerprintJS() {
    if (!Cookies.get('_fpid')) {
        FingerprintJS.load().then(fp => {
            fp.get().then(result => {
                const visitorId = result.visitorId;
                Cookies.set('_fpid', visitorId, {expires: 365});
            });
        });
    }
}

function getApi() {
    let fpId = Cookies.get('_fpid');
    if(fpId !== undefined) {
        Cookies.set('isBlock', 'true')
        axios.post(api_prefix+'/api/get-card', {
            fpId: fpId,
        }).then(function ({data}) {
            Cookies.set('gameSessionId', data.game_id)
            Cookies.set('gameSessionHash', data.game_seesion_hash)
            Cookies.remove('isBlock')

            document.getElementById('myBest').innerHTML = data.myBestScore;
            document.getElementById('globalBest').innerHTML = data.globalBestScore;
        });
    }else{
        location.reload();
    }

}

function newGame() {
    document.querySelectorAll('.is-flipped').forEach(function (el, key) {
        el.classList.remove('is-flipped');
        document.querySelector('.flip' + key + ' > .is-back-' + key).innerHTML = '';
    })
    document.getElementById('totalClick').innerHTML = '0';
    getApi();
}

function submitScore(score) {
    const fpId = Cookies.get('_fpid');
    const gameSessionId = Cookies.get('gameSessionId');
    axios.post(api_prefix+'/api/submit-score', {
        fpid: fpId,
        score: score,
        game_id: gameSessionId,
    }).then(function ({data}) {

    });
}



function sendKey(e) {
    if (Cookies.get('isBlock')) return

    Cookies.set('isBlock', 'true')

    const key = e.getAttribute("data-key");
    const cardEl = document.querySelector('.flip'+key)

    if(cardEl.classList.contains('is-flipped')) {
        Cookies.remove('isBlock')
        return
    } else {
        cardEl.classList.toggle('is-flipped');
    }

    let firstClick = Cookies.get('firstClick');
    let secondClick = Cookies.get('secondClick');

    if (firstClick === undefined) {
        Cookies.set('firstClick', key);
        firstClick = key;

    } else {
        Cookies.set('secondClick', key);
        secondClick = key;
    }

    const fpId = Cookies.get('_fpid');
    const gameId = Cookies.get('gameSessionId');
    const gameSessionHash = Cookies.get('gameSessionHash');

    axios.post(api_prefix+'/api/push-log', {
        fpId: fpId,
        key: key,
        gameId: gameId,
        gameSessionHash: gameSessionHash,
        firstClick: firstClick,
        secondClick: secondClick

    }).then(function ({data}) {
        document.getElementsByClassName("is-back-" + key)[0].innerHTML = data.value;
        document.getElementById('totalClick').innerHTML = data.totalClick;

        if (secondClick !== undefined) {
            if (data.isValid) {
                Cookies.remove('firstClick');
                Cookies.remove('secondClick');

                if(document.querySelectorAll('.is-flipped').length === 12) {
                    submitScore(data.totalClick)
                }
            } else {
                setTimeout(function () {
                    document.querySelector('.flip' + firstClick).classList.remove('is-flipped');
                    document.querySelector('.flip' + firstClick + ' > .is-back-' + firstClick).innerHTML = '';

                    document.querySelector('.flip' + secondClick).classList.remove('is-flipped');
                    document.querySelector('.flip' + secondClick + ' > .is-back-' + secondClick).innerHTML = '';

                    Cookies.remove('firstClick');
                    Cookies.remove('secondClick');
                }, 500);
            }
        }

        Cookies.remove('isBlock')
    });

}


